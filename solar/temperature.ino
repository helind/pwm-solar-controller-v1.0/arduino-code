
const int MosfetTemperaturePin = A0;
const int BatteryTemperaturePin = A1;
const int ExternalTemperaturePin = A5;
#define NTC_R1          10000 //10k
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

//not available
float getMosfetTemperature()
{
  uint16_t NTC_In = analogRead(MosfetTemperaturePin);
  float NTC_R2 = NTC_R1 * (1023.0 / (float)NTC_In - 1.0);
  float logR2 = log(NTC_R2);
  return (float)((1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2)) - 273.15);
}

float getBatteryTemperature()
{
  uint16_t NTC_In = analogRead(BatteryTemperaturePin);
  float NTC_R2 = NTC_R1 * (1023.0 / (float)NTC_In - 1.0);
  float logR2 = log(NTC_R2);
  return (float)((1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2)) - 273.15);
}

//not available
float getExternalTemperature()
{
  uint16_t NTC_In = analogRead(ExternalTemperaturePin);
  float NTC_R2 = NTC_R1 * (1023.0 / (float)NTC_In - 1.0);
  float logR2 = log(NTC_R2);
  return (float)((1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2)) - 273.15);
}


