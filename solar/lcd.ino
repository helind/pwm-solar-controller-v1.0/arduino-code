#include "lcd_icons.h"

//void load_on();
//void load_off();

uint8_t BatteryLevelLCD = 0;

static uint8_t arrow_14_9_bitmap[] U8G_PROGMEM  = {
      0x80, 0x01, 0x80, 0x07, 0xff, 0x0f, 0xff, 0x1f, 0xff, 0x3f, 0xff, 0x1f,
      0xff, 0x0f, 0x80, 0x07, 0x80, 0x01
    };

void draw_sun_panel(void) {
  lcd.drawXBMP(sun_panel_x1, sun_panel_y1, sun_panel_width, sun_panel_height, sun_panel_28_30_bitmap);
}

void draw_no_sun(void) {
  lcd.drawXBMP(no_sun_x1, no_sun_y1, no_sun_width, no_sun_height, no_sun_28_30_bitmap);
}

void draw_battery(void) {
  lcd.drawXBMP(battery_x1, battery_y1, battery_width, battery_height, battery_28_30_bitmap);
  switch (BatteryLevelLCD) {
    case 4:
      lcd.drawHLine(54, 15, 20);
      lcd.drawHLine(54, 16, 20);
    case 3:
      lcd.drawHLine(54, 18, 20);
      lcd.drawHLine(54, 19, 20);
    case 2:
      lcd.drawHLine(54, 21, 20);
      lcd.drawHLine(54, 22, 20);
    case 1:
      lcd.drawHLine(54, 24, 20);
      lcd.drawHLine(54, 25, 20);
  }
}

void draw_load(void) {
  lcd.drawXBMP(load_x1, load_y1, load_width, load_height, load_28_30_bitmap);
}

void draw_arrow1(void) {
  lcd.drawXBMP(arrow1_x1, arrow1_y1, arrow1_width, arrow1_height, arrow_14_9_bitmap);
}

void draw_arrow2(void) {
  lcd.drawXBMP(arrow2_x1, arrow2_y1, arrow2_width, arrow2_height, arrow_14_9_bitmap);
}

void clear_screen(void) {

}

uint8_t load_status = 0;

#define MENU_STEP1     0
#define MENU_STEP2     1
#define MENU_STEP3     2
uint8_t lcd_step = MENU_STEP1;

uint8_t menu_step2_step = 0;
uint8_t menu_step3_step = 0;
uint8_t menu_step3_blinking = 0;
uint8_t menu_step3_blinking_state = 0;
uint8_t menu_step3_change_value = 0;  //1 => +, 2 => -
uint8_t step1_str_numb = 0;
uint8_t button_flag = 0;
uint8_t button_value = 0;

void lcd_interface()
{
  button_value = get_button();
  //  Serial.print(button_value);
  if (lcd_step == MENU_STEP1) {
    //get button command
    if (button_value == BUTTON2_PRESSED_S) {
      if (load_status == 0) {
        load_status = 1;
      } else {
        load_status = 0;
      }
    }

    if (button_value == BUTTONALL_PRESSED_L) {
      lcd_step = MENU_STEP2;
    }

    if (button_value == BUTTON1_PRESSED_S) {
      lcd_step = MENU_STEP3;
    }
  }
  else if (lcd_step == MENU_STEP2) {
    if (button_value == BUTTON1_PRESSED_S) {
      menu_step2_step++;
      if (menu_step2_step == 4) {
        menu_step2_step = 0;
        lcd_step = MENU_STEP1;
      }
    }
  }
  else if (lcd_step == MENU_STEP3) {
    if (button_value == BUTTON1_PRESSED_L) {
      menu_step3_blinking_state = 0;
      if (menu_step3_blinking == 0) menu_step3_blinking = 1;
      else                         menu_step3_blinking = 0;
    }

    if (menu_step3_blinking) {
      if (menu_step3_blinking_state < 5)  menu_step3_blinking_state++;
      else                                menu_step3_blinking_state = 0;
    }

    if (button_value == BUTTON1_PRESSED_S && menu_step3_blinking == 0) {
      menu_step3_blinking_state = 0;
      menu_step3_step++;
      if (menu_step3_step == 4) {
        menu_step3_step = 0;
        lcd_step = MENU_STEP1;
      }
    }

    menu_step3_change_value = 0;
    if (menu_step3_blinking == 1) {
      if (button_value == BUTTON1_PRESSED_S)  menu_step3_change_value = 2;
      if (button_value == BUTTON2_PRESSED_S)  menu_step3_change_value = 1;
    }
  }

  //  Serial.println(button_value);
  BatteryLevelLCD = getBatteryLevel();

  lcd.firstPage();
  do {
    switch (lcd_step) {
      case MENU_STEP1:
        menu_step1();
        show_system_voltage();
        break;
      case MENU_STEP2:
        menu_step2();
        break;
      case MENU_STEP3:
        menu_step3();
        break;
    }

  } while ( lcd.nextPage() );
}

uint32_t step1_str_time = 0;
void step1_str()
{
  if (step1_str_time == 0)
    step1_str_time = millis();

  if (millis() - step1_str_time > 3000 || millis() < step1_str_time) {
    step1_str_numb = (step1_str_numb + 1) % 5;
    step1_str_time = millis();
  }

  switch (step1_str_numb) {
    case 0: //bat voltage
      lcd.setFont(u8g_font_courB08);
      lcd.setPrintPos(2, 52);
      lcd.print("BAT")  ;
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(getBatteryVoltage(), 1);
      lcd.setPrintPos(92, 55);
      lcd.print("V");
      break;
    case 1: //charging voltage - this will be const
      lcd.setFont(u8g_font_courB08);
      lcd.setPrintPos(2, 52);
      lcd.print("PV")  ;
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(getSunPanelVoltage(), 1);
      lcd.setPrintPos(92, 55);
      lcd.print("V");
      break;
    case 2:  //charging current
      lcd.setFont(u8g_font_courB08);
      lcd.setPrintPos(2, 52);
      lcd.print("PV")  ;
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(40.0/*getBatteryCurrent()*/, 1);                                                                       //getBatteryCurrent()
      lcd.setPrintPos(92, 55);
      lcd.print("A");
      break;
    case 3:  //load current
      lcd.setFont(u8g_font_courB08);
      lcd.setPrintPos(2, 52);
      lcd.print("LOAD")  ;
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(40.0/*getLoadCurrent()*/, 1);                                                                           //getLoadCurrent()
      lcd.setPrintPos(92, 55);
      lcd.print("A");
      break;
    case 4:  //bat temperature
      lcd.setFont(u8g_font_courB08);
      lcd.setPrintPos(2, 52);
      lcd.print("BAT")  ;
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(25/*getBatteryTemperature()*/);                                                                         //getBatteryTemperature()
      lcd.setPrintPos(92, 55);
      lcd.print("C");
      lcd.setFont(u8g_font_blipfest_07);//u8g_font_fixed_v0
      lcd.setPrintPos(90, 46);
      lcd.print("o");
      break;
      break;
  }
}

void show_system_voltage()
{
  lcd.setFont(u8g_font_blipfest_07);//u8g_font_fixed_v0
  lcd.setPrintPos(2, 64);
  lcd.print(48 /*getBatteryLevel()*/);                                                                                      //getBatteryLevel()
  lcd.setPrintPos(13, 64);
  lcd.print("V");
}

void menu_step1()
{
  if (isSunPanelConnected())
    draw_sun_panel();
  else
    draw_no_sun();

  draw_battery();

  draw_load();

  if (load_status == 1) {
    load_on();
    draw_arrow2();
  } else {
    load_off();
  }


  /* show arrow if battery is in charging state*/
  if (isBatteryCharging())
    draw_arrow1();
    
  step1_str();

}

void menu_step2()
{
  uint16_t number_of_low_voltage = 0;
  uint16_t working_days = 0;
  uint16_t full_charge_number = 0;
  uint16_t over_current_number = 0;
  char str[4];
  memset(str, 0, 4);
  switch (menu_step2_step) {
    case 0:
      lcd.setFont(u8g_font_fixed_v0);
      lcd.setPrintPos(0, 10);
      lcd.print("LOW VOLTAGE NUMBER");
      sprintf(str, " % 03d", number_of_low_voltage);
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(str);
      break;
    case 1:
      lcd.setFont(u8g_font_fixed_v0);
      lcd.setPrintPos(0, 10);
      lcd.print("WORKING DAYS");
      sprintf(str, " % 03d", working_days);
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(str);
      break;
    case 2:
      lcd.setFont(u8g_font_fixed_v0);
      lcd.setPrintPos(0, 10);
      lcd.print("OVER CURRENT NUMBER");
      sprintf(str, " % 03d", over_current_number);
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(str);
      break;
    case 3:
      lcd.setFont(u8g_font_fixed_v0);
      lcd.setPrintPos(0, 10);
      lcd.print("FULL CHARGE NUMBER");
      sprintf(str, " % 03d", full_charge_number);
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      lcd.print(str);
      break;
  }
}

float load_reconnect_voltage = 0;
float load_disconnect_voltage = 0;
int8_t load_mode = 0;
int8_t bat_type = 0; // 0 = SEL, 1 = GEL, 2 = FLD
void menu_step3()
{
  char str[6];
  memset(str, 0, 6);
  switch (menu_step3_step) {
    case 0:
      show_system_voltage();
      lcd.setFont(u8g_font_fixed_v0);
      lcd.setPrintPos(0, 10);
      lcd.print("LOAD RECONNECT VOLTAGE");
      lcd.setFont(u8g_font_courR14);   //16x26
      if (menu_step3_blinking == 1) {
        if (menu_step3_change_value == 1) {
          if (load_reconnect_voltage < 4.9)
            load_reconnect_voltage += 0.1;
        }

        if (menu_step3_change_value == 2) {
          if (load_reconnect_voltage > 0.05)
            load_reconnect_voltage -= 0.1;
        }
        menu_step3_change_value = 0;
      }
      lcd.setPrintPos(48, 55);
      if (menu_step3_blinking_state < 4)
        lcd.print(load_reconnect_voltage, 1);
      lcd.setPrintPos(92, 55);
      lcd.print("V");
      break;
    case 1:
      show_system_voltage();
      lcd.setFont(u8g_font_fixed_v0);
      lcd.setPrintPos(0, 10);
      lcd.print("LOAD D - CONNECT VOLTAGE");
      if (menu_step3_blinking == 1) {
        if (menu_step3_change_value == 1) {
          if (load_disconnect_voltage < 4.9)
            load_disconnect_voltage += 0.1;
        }

        if (menu_step3_change_value == 2) {
          if (load_disconnect_voltage > 0.05)
            load_disconnect_voltage -= 0.1;
        }
        menu_step3_change_value = 0;
      }
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      if (menu_step3_blinking_state < 4)
        lcd.print(load_disconnect_voltage, 1);
      lcd.setPrintPos(92, 55);
      lcd.print("V");
      break;
    case 2:
      show_system_voltage();
      lcd.setFont(u8g_font_fixed_v0);
      lcd.setPrintPos(0, 10);
      lcd.print("LOAD MODE");
      if (menu_step3_blinking == 1) {
        if (menu_step3_change_value == 1) {
          load_mode += 1;
          load_mode  = load_mode % 25;
        }

        if (menu_step3_change_value == 2) {
          load_mode -= 1;
          if (load_mode < 0)
            load_mode = 24;
        }
        menu_step3_change_value = 0;
      }
      sprintf(str, " % 02d", load_mode);
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      if (menu_step3_blinking_state < 4)
        lcd.print(str);
      lcd.setPrintPos(92, 55);
      lcd.print("h");
      break;
    case 3:
      lcd.setFont(u8g_font_fixed_v0);
      if (menu_step3_blinking == 1) {
        if (menu_step3_change_value == 1) {
          bat_type += 1;
          bat_type  = bat_type % 3;
        }

        if (menu_step3_change_value == 2) {
          bat_type -= 1;
          if (bat_type < 0)
            bat_type = 2;
        }
        menu_step3_change_value = 0;
      }
      lcd.setPrintPos(0, 10);
      lcd.print("BAT TYPE");
      switch (bat_type) {
        case 0:
          sprintf(str, "SEL");
          break;
        case 1:
          sprintf(str, "GEL");
          break;
        case 2:
          sprintf(str, "FLD");
          break;
      }
      lcd.setFont(u8g_font_courR14);   //16x26
      lcd.setPrintPos(48, 55);
      if (menu_step3_blinking_state < 4)
        lcd.print(str);
      break;
  }
}



