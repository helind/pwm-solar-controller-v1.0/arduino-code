#define BatteryCurrentPin             A4  //charge
#define LoadCurrentPin                A5

// scale factor => mVperAmp 
// Offset       => ACSoffset

// chose chip-type
// #define ACS_50U  // 50A unidirectional
#define ACS_50B  // 50A bi-directional
// #define ACS_100U
// #define ACS_100B
// #define ACS_150U
// #define ACS_150B
// #define ACS_200U
// #define ACS_200B

#ifdef ACS_50U
#define  mVperAmp           60
#define  ACSoffset          600
#elif defined ACS_50B
#define  mVperAmp           40
#define  ACSoffset          2500
#elif defined ACS_100U
#define  mVperAmp           40
#define  ACSoffset          600
#elif defined ACS_100B
#define  mVperAmp           20
#define  ACSoffset          2500
#elif defined ACS_150U
#define  mVperAmp           26.7
#define  ACSoffset          600
#elif defined ACS_150B
#define  mVperAmp           13.3
#define  ACSoffset          2500
#elif defined ACS_200U
#define  mVperAmp           20
#define  ACSoffset          600
#elif defined ACS_200B
#define  mVperAmp           10
#define  ACSoffset          2500
#else
#error "choose asc758 type"
#endif

void acs758_init(void)
{
  pinMode(BatteryCurrentPin,INPUT);
  pinMode(LoadCurrentPin,INPUT);
}

float getBatteryCurrent()
{
  uint16_t RawValue= 0;
  float Voltage = 0;
  RawValue = analogRead(BatteryCurrentPin);
  Voltage = (RawValue / 1023.0) * 5000;
  return ((Voltage - ACSoffset) / mVperAmp);
}

float getLoadCurrent()
{
  uint16_t RawValue= 0;
  float Voltage = 0;
  RawValue = analogRead(LoadCurrentPin);
  Voltage = (RawValue / 1023.0) * 5000;
  return ((Voltage - ACSoffset) / mVperAmp);
}


 
// Serial.print("Raw Value = " ); // shows pre-scaled value 
// Serial.print(RawValue); 
// Serial.print("\t mV = "); // shows the voltage measured 
// Serial.print(Voltage,3); // the '3' after voltage allows you to display 3 digits after decimal point
// Serial.print("\t Amps = "); // shows the voltage measured 
// Serial.println(Amps,3); // the '3' after voltage allows you to display 3 digits after decimal point
// delay(2500); 
 
