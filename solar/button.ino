#define BUTTON1             2
#define BUTTON2             3


void button_init()
{
  pinMode(BUTTON1, INPUT);
  pinMode(BUTTON2, INPUT);
}


uint32_t Button1_Time = 0;
uint32_t Button2_Time = 0;

#define BUTTON_NONE           0
#define BUTTON1_PRESSED_L     1
#define BUTTON2_PRESSED_L     2
#define BUTTONALL_PRESSED_L   3
#define BUTTON1_PRESSED_S     4
#define BUTTON2_PRESSED_S     5

uint8_t button_pressed = 0;
uint8_t get_button()
{
 /*
  if (button_pressed == 0) {
  */
    if (Button1_Time > 2900 && Button2_Time > 2900)
      return BUTTONALL_PRESSED_L;

    if (Button1_Time > 2900)
      return BUTTON1_PRESSED_L;

    if (Button2_Time > 2900)
      return BUTTON2_PRESSED_L;

    if (Button1_Time > 90)
      return BUTTON1_PRESSED_S;

    if (Button2_Time > 90)
      return BUTTON2_PRESSED_S;
 /*
  }
  */
  return 0;
}

void read_button()
{
  Button1_Time = 0;
  Button2_Time = 0;
 /*
  if (button_pressed == 0) {
  */
    while (digitalRead(BUTTON1) == LOW) {
      delay(50);
      if (digitalRead(BUTTON2) == LOW) {
        Button2_Time += 50;
      }
      Button1_Time += 50;
    }
    while (digitalRead(BUTTON2) == LOW) {
      delay(50);
      if (digitalRead(BUTTON1) == LOW) {
        Button1_Time += 50;
      }
      Button2_Time += 50;
    }
 /*
  }
 */

/*
  if ((digitalRead(BUTTON1) == HIGH) && (digitalRead(BUTTON2) == HIGH))
    button_pressed = 0;
*/
}

