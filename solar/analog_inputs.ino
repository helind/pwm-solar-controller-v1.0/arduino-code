#define BatteryVoltagePin               A6
#define SunPanelVoltagePin              A3
#define SunPanelVoltagePin2             A9

#define SunPanelVoltageThreshold  0
#define isSunPanelConnected()   (getSunPanelVoltage() >  SunPanelVoltageThreshold) ? 1:0

#define BATTERY_12V_LEVEL_1    10         //10V  //not correct values
#define BATTERY_12V_LEVEL_2    10.5
#define BATTERY_12V_LEVEL_3    11
#define BATTERY_12V_LEVEL_4    11.5

/*
 * Batereyanin charge olmagi pwm-in tedbiqinden sonra tamamlanacaq. indilik lcd-de goruntunu test etmek ucun
 * bele tedbiq etdim. Sonra normal bir funksiya yazilmalidi.
 */
#define isBatteryCharging()       1

uint8_t getBatteryLevel()
{
  uint8_t bat_type = getSystemVoltageType();
  float bat_voltage = getBatteryVoltage();
  switch (getSystemVoltageType())
  {
    case 48:
      bat_voltage /= 4;
      break;
    case 24:
      bat_voltage /= 2;
      break;
  }

  return (bat_voltage > BATTERY_12V_LEVEL_4 ? 4 :
         (bat_voltage > BATTERY_12V_LEVEL_3 ? 3 :
         (bat_voltage > BATTERY_12V_LEVEL_2 ? 2 :
         (bat_voltage > BATTERY_12V_LEVEL_1 ? 1 : 0))));
}

float getBatteryVoltage()
{
  uint16_t RawValue = 0;
  float Voltage = 0;
  RawValue = analogRead(BatteryVoltagePin);
  Voltage = (RawValue / 1023.0) * 5;
  Voltage = Voltage / 10 * 210 / 1.066; //these values are from experimental calculations. Resistor values may change in future. please reconsider them when new board is used.
  return Voltage;
}

float getSunPanelVoltage()
{
  uint16_t RawValue = 0;
  uint16_t RawValue2 = 0;
  float Voltage = 0;
  RawValue = analogRead(SunPanelVoltagePin);
  RawValue2 = analogRead(SunPanelVoltagePin2);
  Serial.print(RawValue);
  Serial.print(" AND ");
  Serial.println(RawValue2);
  Voltage = ((RawValue2-RawValue) / 1023.0) * 5;
  Voltage = Voltage / 10 * 210 / 1.066; //these values are from experimental calculations. Resistor values may change in future. please reconsider them when new board is used.
  return RawValue;
}


/**
 * @ ret values: 0, 12 , 24 or 48
 * @info:        if battery voltage is under 9V, return value will be 0
 */
uint8_t getSystemVoltageType()
{
  float bat_voltage = getBatteryVoltage();
  return (bat_voltage > 40 ? 48 :
          (bat_voltage > 18 ? 24 :
           (bat_voltage > 9  ? 12 : 0)));
}

