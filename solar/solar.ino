#define F_CPU 16000000UL
#include "U8glib.h"
#include <avr/interrupt.h>

/*********************************************************************
 * Project name:              SOLAR TRACKER
 * Author:                    Majid Ahmadov
 * Code Last Edit Date:       27.02.2020          
 * Info Last Edir Date:       16.08.2020
 * 
 *          * ACS758 * 
 *  - acs758.ino faylinda ACS758 tipini secmek lazimdir.
 *    bu secim vaxti diqqet edilmelidi:
 *      1. bi-directional veya unidirectional olmagina
 *      2. Olcme heddi
 *    bizim numunede 50A bi-directional istifade edilmisdi.
 *  - acs758.ino faylinda ACS758 analog pinleri secilmelidi. 
 *    Bu pinler:
 *      1. BatteryCurrentPin
 *      2. LoadCurrentPin
 *  
 *        * BATTERY VOLTAGE *
 *  - analog_input.ino faylinda BatteryVoltagePin secilir.
 *  - getBatteryVoltage funksiyasi bateriyanin voltajini verir.
 *    bu funksiyada yeni boarddaki resistor ferqliliklerine esasen deyisiklik etmek lazimdi!
 *  - getBatteryLevel batereyani 4 skala seviyyesinde qiymetini verir
 *  - getSystemVoltageType funksiyasi batereya voltajina gore sistemin tipini mueyyen edir.
 *    0, 12, 24 ve ya 48. 0 aldiqda, batereyanin qosulmadigini veya problem oldugunu tedbiq etmek olar
 *  
 *        * SUN PANEL VOLTAGE *
 *  - analog_input.ino faylinda SunPanelVoltagePin ve SunPanelVoltagePin2 pinleri secilir.
 *  - getSunPanelVoltage funksiyasi bu iki pin arasindaki gerginlik ferqini hesablayir.
 *    bu funksiyada da adcni duzgun scale etmek ucun yeni boarda nezeren deyisikler etmek lazimdir
 *    
 *         * BUTTONS *
 * - button.ino faylinda button pinleri secilir. Pinler active low olaraq oxunur.
 * - buttonlar interrupt ile tedbiq edilmeyib. loopda her defe oxunur. interrupt veya timer ile tedbiqi daha yaxsi olardi.
 * - read_button funksiyasi pinin low levelde olma muddetini teyin edir.
 * - get_button funksiyasi lcd_interface funksiyasinda cagirilir. ve buttonda deysiklik olubsa xeber verir.
 *      1. BUTTON_NONE       
 *      2. BUTTON1_PRESSED_L     
 *      3. BUTTON2_PRESSED_L     
 *      4. BUTTONALL_PRESSED_L   
 *      5. BUTTON1_PRESSED_S     
 *      6. BUTTON2_PRESSED_S     
 *         
 *        * LCD ICONS *   
 * - Burada LCD ucun istifade edilen iconlarin datalari movcuddur.
 * 
 *        * LCD *
 * - 3 menu var.
 *   Menu 1 => button_2 loadi acib baglayacaq
 *          => button_1 Menu_2 ye kecid
 *          => button_1 + button_2 uzun basdiqda Menu_3 e kecid
 *          
 *   Menu 2 => 4 sehifeden ibaretdi. button_1 ile sehifeleri deyismek olar. Bu menulara menu_step2 funksiyasinda
 *             baxmaq olar.
 *   
 *   Menu 3 => belli parametrleri ayarlamaq ucun sehifelere bolunub. menu_step3 funksiyasinda bunlar verilib.
 *   
 *        * PWM *
 * - PWM pinleri sadece test meqsedi ile istifade olunub. boardda seflikler oldugu ucun neticelene bilmeyib.       
 *   yeni versiyada hazirlanmali idi.
 *        
 *        *TEMPERATURE *
 * - temperature.ino faylinda BatteryTemperaturePin pini secilir. 
 * - getBatteryTemperature funksiyasi ile baterayanin uzerine yerlesdirilmis NTC-den
 *   temperatur datasi almaq olar. diger NTC pinleri test esasinda boardda movcud olmadigi 
 *   ucun yoxlanilmayib.
 * 
 *         
 * 
 */


/*loop periodunu sabit saxlamaq ucun*/
#define LOOP_PERIOD_TIME          1000 //ms
#define LOOP_TIME_SPEND()         (millis() > loop_time ) ? abs(millis() - loop_time):(LOOP_PERIOD_TIME) //loop-da kecirilen vaxt
#define LOOP_TIME_REFRESH()       loop_time = millis() //loop vaxtini yenilemek ucun
uint32_t loop_time = 0;


/* LCD Pins*/
#define LCD_CONTRAST              10 //13//52 //
#define EN                        52 //13//52  //lcd pin6//SCK
#define RW                        50 //12//50 //lcd pin5//MISO
#define RS                        51  //11//51 //lcd pin4//MOSI  reset
#define LCD_LIGHT                 7  //11//51 //parlaqligi pwmle idare etmek olar 
U8GLIB_ST7920_128X64_1X lcd(EN, RW, RS);



void setup(void) {
  Serial.begin(115200);

  /*init lcd*/
  if ( lcd.getMode() == U8G_MODE_R3G3B2 ) {
    lcd.setColorIndex(255);
  }
  else if ( lcd.getMode() == U8G_MODE_GRAY2BIT ) {
    lcd.setColorIndex(3);
  }
  else if ( lcd.getMode() == U8G_MODE_BW ) {
    lcd.setColorIndex(1);
  }
  lcd.setRot180();
  pinMode(LCD_LIGHT, OUTPUT);
  pinMode(LCD_CONTRAST, OUTPUT);

  acs758_init();
  
  button_init();
  
  pwm_init();

  /*enabled global interrupts*/
  sei();

  /*start loop timer*/
  loop_time = millis();
}

void loop(void) {
  read_button();
  lcd_interface();
  Serial.print("BAT VOLTAGE      :   "); Serial.println(getBatteryVoltage(), 1);
  Serial.print("BAT CURRENT      :   "); Serial.println(getBatteryCurrent(), 1);
  Serial.print("SUN PANEL VOLTAGE:   "); Serial.println(getSunPanelVoltage(), 1);
  Serial.print("LOAD CURRENT     :   "); Serial.println(getLoadCurrent(), 1);
//  Serial.print("MOSFET TEMP.     :   "); Serial.println(getMosfetTemperature(), 1);
//  Serial.print("BAT TEMP.        :   "); Serial.println(getBatteryTemperature(), 1);
  Serial.print("EXTERNAL TEMP.   :   "); Serial.println(getExternalTemperature(), 1);


  if(LOOP_TIME_SPEND() > 5000){ //eger gozlenilenden cox vaxt kecibse xeta bildirmek olar (veya watchdog elave etmek olar)
    /*FAULT*/
  }else{
    while(LOOP_TIME_SPEND() < LOOP_PERIOD_TIME){  //period tamamlanana kimi gozle. hemin vaxtda buttonda deyisiklik varsa oxu
        read_button();
    }
  }
  LOOP_TIME_REFRESH();
}

/*
ISR(TIMER1_COMPA_vect)
{
  PORTE |= _BV(PORTE0);
  PORTE |= _BV(PORTE0);
  static uint32_t counter = 0, cnt = 0;
  counter++;
  cnt++;
  PORTE |= _BV(PORTE0);
  PORTE &= ~_BV(PORTE0);
}
*/
