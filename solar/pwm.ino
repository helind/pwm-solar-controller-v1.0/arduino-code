void pwm_init()
{
  /*load*/
  DDRH |= 0x08;
  TCCR4A = TCCR4A & 0b00000000;
  TCCR4B = TCCR4B & 0b00100000;
  TCCR4A = TCCR4A | 0b10000011;
  TCCR4B = TCCR4B | 0b00001001;
  OCR4A = 95;

  DDRL |= 0x28; //pl5 pl3
  /* 44 , 46 charging */
  TCCR5A = TCCR5A & 0b00000000;
  TCCR5B = TCCR5B & 0b00100000;
  TCCR5A = TCCR5A | 0b10001011;
  TCCR5B = TCCR5B | 0b00001001;
  OCR5A = 0;
  OCR5C = 0;
}

void pwm_init2()
{
  
    DDRE |= 0x03;   //PE0 PE1 test
    DDRH |= 0x48; //PH3&PH6 = 15&18 = D6&D9


    //CS(preskalar) 000 = stop , 001 = no , 010 = 8
    //TCCR3B = ICNC3 ICES3 – WGM33 WGM32 CS32 CS31 CS30
    //TCCR3A = COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30
    /*  WGMn3 WGMn2  WGMn1  WGMn0
        5- 0    1     0     1 Fast PWM, 8-bit  0x00FF BOTTOM TOP
        6- 0    1     1     0 Fast PWM, 9-bit  0x01FF BOTTOM TOP
        7- 0    1     1     1 Fast PWM, 10-bit 0x03FF
  */
  // ## set timer 0 -> prescalar ->1,  fast pwm WGM0[2:0] = 011 , COM0A[1:0] = 10
  //    TCCR3A |= (1<<COM3A1) | (0<<COM3A0) | (1<<WGM31) | (1<<WGM30);
  //    TCCR3B |= (1<<WGM32)  | (0<<CS31)   | (1<<CS30);
  //    TCCR3A = TCCR3A & 0b10000011;
  //    TCCR3B = TCCR3B & 0b00101001;
  DDRE |= 0x08; //PE3 = 5 = D5
  DDRE |= 0x10; //PE3 = 6 = D2
  TCCR3A = TCCR3A & 0b00000000;
  TCCR3B = TCCR3B & 0b00100000;
  TCCR3A = TCCR3A | 0b10100011;
  TCCR3B = TCCR3B | 0b00001001;
  OCR3A = 512;

  TCCR5A = TCCR5A & 0b00000000;
  TCCR5B = TCCR5B & 0b00100000;
  TCCR5A = TCCR5A | 0b10101011;
  TCCR5B = TCCR5B | 0b00001001;
  OCR5A = 200;

  TCCR4A = TCCR4A & 0b00000000;
  TCCR4B = TCCR4B & 0b00100000;
  TCCR4A = TCCR4A | 0b10101011;
  TCCR4B = TCCR4B | 0b00001001;
  OCR4A = 333;

  //    TCCR4A |= (1<<COM4A1) | (0<<COM4A0) | (1<<WGM41) | (1<<WGM40);
  //    TCCR4B |= (1<<WGM42)  | (1<<CS41)   | (0<<CS40);
  //    TCCR4A = TCCR4A & 0b10000011;
  //    TCCR4B = TCCR4B & 0b00101001;
  //    OCR4A = 200;

  // ## set timer 1  -> prescalar -> 8 CS11=1, WGM1[3:0] = 0100 , enable interrupt for OCIE1A
  TCCR1B |= (1 << WGM12) /*ctc mode*/ | (1 << CS11) | (1 << CS10) | (0 << CS12); //2mhz timer clock
  TIMSK1 |= (1 << OCIE1A); //enable interrupt when counter matches the OC1RA
  TCCR1A = TCCR1A & 0b00000000;
  TCCR1B = TCCR1B & 0b00101001;
  OCR1A  = 100; //counter is equal to 1000
  
}

void load_on()
{

}

void load_off()
{

}

void battery_charge_on()
{

}

void battery_charge_off()
{

}

